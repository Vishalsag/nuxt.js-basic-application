# This application is created as demo to learn Nuxt.js  

# recipes

> My awesome recipe book app

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

# To start your own app with nuxt.js  
```bash
# It will ask for password
$ sudo npm install -g create-nuxt-app

# Create app with your application name
$ create-nuxt-app <name-of-your-application>

# Select the configuration you would like to have for your project

# It will display the get started page

# Go to the directory with your application name
$ cd <name>

# Run the server which will listen at http://localhost:3000
$ npm run dev

# Start coding your application
```

Reference taken from [Link](https://www.youtube.com/watch?v=nteDXuqBfn0)  
For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
